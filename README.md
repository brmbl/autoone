# Auto1 Coding Challenge
##### Simple API application

The easiest way to start using is to use docker.

So, to start this application, you need to do following steps:

- Run from the project root:

```
docker-compose build
docker-compose up -d
docker-compose run php composer install"
```
This application use Nelmio API doc bundle, so you can find pretty docummentation 
of the API structure on the index page:

- Open [http://localhost:8001](http://localhost:8001);

Also you can run unit tests:

```
docker-compose run php /www/vendor/bin/phpunit
```

#PS
I choose next parameters to match the best vacancy for me :
```
http://localhost:8001/vacancies/match/?skills=PHP,SQL,PHPUnit,Symfony,Mycroservices,Docker,REST
```
And check this out :)
```
{
    "ID": "1",
    "Job title": "Senior PHP Developer",
    "Seniority level": "Senior",
    "Country": "DE",
    "City": "Berlin",
    "Salary": "747500",
    "Currency": "SVU",
    "Required skills": "PHP, Symfony, REST, Unit-testing, Behat, SOLID, Docker, AWS",
    "Company size": "100-500",
    "Company domain": "Automotive"
}
```

Very interesting and exciting challenge :)
