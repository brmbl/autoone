<?php

namespace App\Tests\Infrastructure\DBAL\FileManagement;

use App\Infrastructure\DBAL\Configuration\CsvConfiguration;
use App\Infrastructure\DBAL\FileManagement\CsvManager;
use App\Infrastructure\Exception\ConfigurationException;
use PHPUnit\Framework\TestCase;

class CsvManagerTest extends TestCase
{
    /** @var CsvManager */
    private $csvManager;

    /**
     * @throws ConfigurationException
     */
    public function setUp(){
        parent::setUp();

        $csvConfig = new CsvConfiguration(['file_path' => __DIR__ . '/test.csv']);
        $this->csvManager = new CsvManager($csvConfig);
    }

    /**
     * @param array $criteria
     * @param array $ordering
     * @param int $expectedCount
     *
     * @dataProvider criteriaDataProvider
     * @throws \Exception
     */
    public function testItReadBy(array $criteria, array $ordering, int $expectedCount)
    {
        //given
        //when
        $data = $this->csvManager->readBy($criteria, $ordering);
        $actualCount = count($data);

        //then
        $this->assertEquals($expectedCount, $actualCount);
    }

    /**
     * @return array
     */
    public function criteriaDataProvider(): array
    {
        $criteria1 = ['Country' => 'DE', 'City' => 'Berlin'];
        $ordering1 = ['Seniority level'];
        $count1 = 3;

        $criteria2 = ['ID' => 4];
        $ordering2 = [];
        $count2 = 1;

        $criteria3 = ['Country' => 'SE'];
        $ordering3 = ['Salary'];
        $count3 = 2;

        return [
            [$criteria1, $ordering1,$count1],
            [$criteria2, $ordering2,$count2],
            [$criteria3, $ordering3,$count3],
        ];
    }
}