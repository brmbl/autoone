<?php

namespace App\Tests\Infrastructure\DBAL\Configuration;


use App\Infrastructure\DBAL\Configuration\DBALConfiguration;
use App\Infrastructure\Exception\ConfigurationException;
use PHPUnit\Framework\TestCase;

class DBALConfigurationTest extends TestCase
{
    /**
     * @throws ConfigurationException
     */
    public function testGoodConfiguration()
    {
        //given
        $params = ['driver' => 'test-driver', 'configuration' => 'test-configuration'];

        //when
        $dbalConfiguration = new DBALConfiguration($params);

        //then
        $this->assertEquals('test-driver', $dbalConfiguration->getDriver());
        $this->assertEquals($params, $dbalConfiguration->getParams());
    }

    /**
     * @throws ConfigurationException
     */
    public function testBadConfiguration()
    {
        //given
        $params = ['driver-not-exists' => 'test-driver', 'configuration' => 'test-configuration'];

        //then
        $this->expectException(ConfigurationException::class);

        //when
        new DBALConfiguration($params);
    }
}