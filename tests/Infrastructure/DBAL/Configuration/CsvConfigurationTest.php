<?php

namespace App\Tests\Infrastructure\DBAL\Configuration;


use App\Infrastructure\DBAL\Configuration\CsvConfiguration;
use App\Infrastructure\Exception\ConfigurationException;
use PHPUnit\Framework\TestCase;

class CsvConfigurationTest extends TestCase
{
    /**
     * @throws \App\Infrastructure\Exception\ConfigurationException
     */
    public function testGoodConfiguration()
    {
        //given
        $filePath = 'some/file/path';
        $configuration = ['file_path' => $filePath];

        //when
        $configuration = new CsvConfiguration($configuration);

        //then
        $this->assertEquals($filePath, $configuration->getFilePath());
    }

    /**
     * @throws ConfigurationException
     */
    public function testBadConfiguration()
    {
        //given
        $configuration = ['file_path_not_exists' => 'wrong-configuration'];

        //then
        $this->expectException(ConfigurationException::class);

        //when
        new CsvConfiguration($configuration);
    }
}