<?php

namespace App\Tests\Infrastructure\Service;


use App\Domain\Entity\Vacancy;
use App\Infrastructure\Service\VacancyMatchingCalculator;
use PHPUnit\Framework\TestCase;

class VacancyMatchingCalculatorTest extends TestCase
{
    /**
     * @dataProvider vacancyDataProvider
     */
    public function testItMatchVacancy($vacancies, $skills, $expectedMatch)
    {
        $calculator = new VacancyMatchingCalculator();

        $matchedVacancy = $calculator->calculateBySkills($vacancies, $skills);
        $actualMatch = implode(',', $matchedVacancy->getRequiredSkills());

        $this->assertEquals($actualMatch, $expectedMatch);
    }

    /**
     * @return array
     */
    public function vacancyDataProvider()
    {
        $vacancy1 = (new Vacancy())->setRequiredSkills('PHP, Symfony, Unit-testing, SOLID, CSS, SQL');
        $vacancy2 = (new Vacancy())->setRequiredSkills('Java, Spring, REST, Microservices, Kafka, Hibernate');
        $vacancy3 = (new Vacancy())->setRequiredSkills('PHP, JavaScript, SASS/LESS, SCRUM, ReactJS, Redux, NPM, Yarn');
        $vacancy4 = (new Vacancy())->setRequiredSkills('Node.js, JavaScript, JIRA, Bamboo, webpack');
        $vacancy5 = (new Vacancy())->setRequiredSkills('Java, Spring, Microservices, Kinesis, Junit, SOAP/RPC, Hibernate');
        $vacancy6 = (new Vacancy())->setRequiredSkills('PHP, Falcon, REST, SQL, MongoDB, Unit-testing, Behat, SOLID, Docker, AWS');
        $vacancy7 = (new Vacancy())->setRequiredSkills('PHP, JavaScript, SASS/LESS, SCRUM, ReactJS, Redux, NPM, Yarn');
        $vacancy8 = (new Vacancy())->setRequiredSkills('Node.js, JavaScript, Git, Mongo, noSQL, NPM');
        $vacancy9 = (new Vacancy())->setRequiredSkills('PHP, JavaScript, CSS/SASS, Angular');

        $skills1 = 'PHP,Symfony,Unit-testing,SOLID,CSS,SQL,LESS,AWS,Angular,MongoDB,Docker';
        $skills2 = 'Java, Spring, Microservices, Kinesis, Junit, SOAP/RPC, Hibernate';
        $skills3 = 'PHP,Microservices,Unit-testing,Java,CSS,SQL,Falcon,AWS,Angular,JavaScript,Docker';

        $match1 = 'PHP, Symfony, Unit-testing, SOLID, CSS, SQL';
        $match2 = 'Java, Spring, Microservices, Kinesis, Junit, SOAP/RPC, Hibernate';
        $match3 = 'PHP, JavaScript, CSS/SASS, Angular';
        return [
            [
                [
                    $vacancy1,
                    $vacancy2,
                    $vacancy3,
                ],
                $skills1,
                $match1,
            ],
            [
                [
                    $vacancy4,
                    $vacancy5,
                    $vacancy6,
                ],
                $skills2,
                $match2,
            ],
            [
                [
                    $vacancy7,
                    $vacancy8,
                    $vacancy9,
                ],
                $skills3,
                $match3,
            ],
        ];
    }
}