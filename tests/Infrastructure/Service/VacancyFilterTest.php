<?php

namespace App\Tests\Infrastructure\Service;


use App\Domain\Entity\Vacancy;
use App\Infrastructure\ORM\Hydrator\ObjectHydrator;
use App\Infrastructure\Service\VacancyFilter;
use PHPUnit\Framework\TestCase;

class VacancyFilterTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testItOrderSeniorityLevel()
    {
        //given
        $data = [
            [
                'ID' => '1',
                'Job title' => 'Senior PHP Developer',
                'Seniority level' => 'Senior',
                'Country' => 'DE',
                'City' => 'Berlin',
                'Salary' => '747500',
                'Currency' => 'SVU',
                'Required skills' => 'PHP, Symfony, REST, Unit-testing, Behat, SOLID, Docker, AWS',
                'Company size' => '100-500',
                'Company domain' => 'Automotive',
            ],
            [
                'ID' => '2',
                'Job title' => 'Middle PHP Developer',
                'Seniority level' => 'Middle',
                'Country' => 'DE',
                'City' => 'Berlin',
                'Salary' => '632500',
                'Currency' => 'SVU',
                'Required skills' => 'PHP, Symfony, Unit-testing, SOLID',
                'Company size' => '100-500',
                'Company domain' => 'Automotive',
            ],
            [
                'ID' => '3',
                'Job title' => 'Junior PHP Developer',
                'Seniority level' => 'Junior',
                'Country' => 'DE',
                'City' => 'Berlin',
                'Salary' => '517500',
                'Currency' => 'SVU',
                'Required skills' => 'PHP, LAMP, HTML, CSS, SQL',
                'Company size' => '100-500',
                'Company domain' => 'Automotive',
            ],
            [
                'ID' => '4',
                'Job title' => 'Senior Java Developer',
                'Seniority level' => 'Senior',
                'Country' => 'DE',
                'City' => 'Hamburg',
                'Salary' => '897000',
                'Currency' => 'SVU',
                'Required skills' => 'Java, Spring, REST, Microservices, Kafka, Hibernate',
                'Company size' => '50-100',
                'Company domain' => 'Real Estate',
            ],
            [
                'ID' => '5',
                'Job title' => 'Senior Fullstack Developer',
                'Seniority level' => 'Senior',
                'Country' => 'DE',
                'City' => 'Berlin',
                'Salary' => '839500',
                'Currency' => 'SVU',
                'Required skills' => 'PHP, JavaScript, CSS/SASS, PHPUnit, Karma, Jenkins',
                'Company size' => '50-100',
                'Company domain' => 'FinTech',
            ]
        ];
        $hydrator = new ObjectHydrator();
        $vacancies = $hydrator->hydrateAll($data, new Vacancy());
        $filter = new VacancyFilter();

        //when
        $filtered = $filter->orderWithSeniority($vacancies, ["seniorityLevel"]);

        //then
        $this->assertEquals($filtered[0]->getSeniorityLevel(), "Junior");
        $this->assertEquals($filtered[4]->getSeniorityLevel(), "Senior");
    }
}