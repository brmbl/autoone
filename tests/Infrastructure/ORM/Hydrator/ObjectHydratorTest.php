<?php

namespace App\Tests\Infrastructure\ORM\Hydrator;


use App\Domain\Entity\Vacancy;
use App\Infrastructure\ORM\Hydrator\ObjectHydrator;
use PHPUnit\Framework\TestCase;

class ObjectHydratorTest extends TestCase
{
    /** @var ObjectHydrator */
    private $objectHydrator;

    public function setUp()
    {
        parent::setUp();

        $this->objectHydrator = new ObjectHydrator();
    }


    public function testItHydrate()
    {
        //given
        $entity = new Vacancy();
        $data = [
            'ID' => '4',
            'Job title' => 'Senior Java Developer',
            'Seniority level' => 'Senior',
            'Country' => 'DE',
            'City' => 'Hamburg',
            'Salary' => '897000',
            'Currency' => 'SVU',
            'Required skills' => 'Java, Spring, REST, Microservices, Kafka, Hibernate',
            'Company size' => '50-100',
            'Company domain' => 'Real Estate',
        ];

        //when
        $this->objectHydrator->hydrate($data, $entity);

        //then
        $this->assertEquals($data['ID'], $entity->getId());
        $this->assertEquals($data['Job title'], $entity->getJobTitle());
        $this->assertEquals($data['Seniority level'], $entity->getSeniorityLevel());
        $this->assertEquals($data['Country'], $entity->getCountry());
        $this->assertEquals($data['City'], $entity->getCity());
        $this->assertEquals($data['Salary'], $entity->getSalary());
        $this->assertEquals($data['Currency'], $entity->getCurrency());
        $this->assertEquals($data['Required skills'], implode(',', $entity->getRequiredSkills()));
        $this->assertEquals($data['Company size'], $entity->getCompanySize());
        $this->assertEquals($data['Company domain'], $entity->getCompanyDomain());
    }

    public function testItExtract()
    {
        //given
        $entity = (new Vacancy())->setID('4')
            ->setJobTitle('Senior Java Developer')
            ->setSeniorityLevel('Senior')
            ->setCountry('DE')
            ->setCity('Hamburg')
            ->setSalary('897000')
            ->setCurrency('SVU')
            ->setRequiredSkills('Java, Spring, REST, Microservices, Kafka, Hibernate')
            ->setCompanySize('50-100')
            ->setCompanyDomain('Real Estate');
        $array = $data = [
            'ID' => '4',
            'Job title' => 'Senior Java Developer',
            'Seniority level' => 'Senior',
            'Country' => 'DE',
            'City' => 'Hamburg',
            'Salary' => '897000',
            'Currency' => 'SVU',
            'Required skills' => explode(',', 'Java, Spring, REST, Microservices, Kafka, Hibernate'),
            'Company size' => '50-100',
            'Company domain' => 'Real Estate',
        ];

        //when
        $arrayData = $this->objectHydrator->extract($entity);

        //then
        $this->assertEquals(array_values($array), array_values($arrayData));
    }
}