<?php

namespace App\Tests\Infrastructure\ORM\Persister;

use App\Domain\Entity\Vacancy;
use App\Infrastructure\DBAL\FileManagement\CsvManager;
use App\Infrastructure\ORM\EntityManager;
use App\Infrastructure\ORM\Hydrator\ObjectHydrator;
use App\Infrastructure\ORM\Persister\CsvPersister;
use PHPUnit\Framework\TestCase;

class CsvPersisterTest extends TestCase
{
    /** @var CsvManager */
    private $csvManagerMock;

    /** @var EntityManager */
    private $emMock;

    public function setUp()
    {
        parent::setUp();

        $this->csvManagerMock = $this->getMockBuilder(CsvManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->emMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->emMock->expects($this->once())->method('getHydrator')
            ->will($this->returnValue(new ObjectHydrator()));
        $this->emMock->expects($this->once())->method('getEntity')
            ->will($this->returnValue(new Vacancy()));
    }

    public function testItLoadById()
    {
        //given
        $id = 123;
        $this->csvManagerMock->expects($this->once())->method('readBy')->with(["ID" => $id])
            ->will($this->returnValue([
                ['ID' => $id, 'Country' => 'DE', 'City' => 'Berlin'],
            ]));
        $csvPersister = new CsvPersister($this->emMock, $this->csvManagerMock);

        //when
        $result = $csvPersister->loadById($id);

        //then
        $this->assertTrue($result instanceof Vacancy);
        $this->assertEquals($id, $result->getId());
    }

    /**
     * @throws \Exception
     */
    public function testItLoadAll()
    {
        //given
        $this->csvManagerMock->expects($this->once())->method('readBy')->with([], [])
            ->will($this->returnValue([
                ['ID' => 123, 'Country' => 'DE', 'City' => 'Berlin'],
                ['ID' => 456, 'Country' => 'IT', 'City' => 'Milan'],
            ]));

        $csvPersister = new CsvPersister($this->emMock, $this->csvManagerMock);

        //when
        $result = $csvPersister->loadAll();

        //then
        $entity1 = $result[0];
        $entity2 = $result[1];
        $this->assertTrue($entity1 instanceof Vacancy);
        $this->assertTrue($entity2 instanceof Vacancy);
        $this->assertEquals(123, $entity1->getID());
        $this->assertEquals(456, $entity2->getID());
    }
}