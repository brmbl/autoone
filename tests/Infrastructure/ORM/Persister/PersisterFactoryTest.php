<?php

namespace App\Tests\Infrastructure\ORM\Persister;


use App\Infrastructure\DBAL\Configuration\DBALConfiguration;
use App\Infrastructure\ORM\EntityManager;
use App\Infrastructure\ORM\Persister\CsvPersister;
use App\Infrastructure\ORM\Persister\PersisterFactory;
use PHPUnit\Framework\TestCase;

class PersisterFactoryTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testItCreatePersister()
    {
        //given
        $configuration = new DBALConfiguration(['driver' => 'csv', 'file_path' => 'some-path']);
        $em = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $em->expects($this->any())->method('getConfiguration')->will($this->returnValue($configuration));

        //when
        $persister = PersisterFactory::createPersister($em);

        //then
        $this->assertTrue($persister instanceof CsvPersister);
    }
}