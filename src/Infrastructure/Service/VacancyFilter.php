<?php

namespace App\Infrastructure\Service;

use App\Domain\Entity\Vacancy;
use App\Infrastructure\ORM\Hydrator\ObjectHydrator;

class VacancyFilter
{
    const SENIORITY_LEVEL_FIELD = 'Seniority level';
    const SENIORITY_LEVEL_KEY = 'seniorityLevel';

    /** @var array */
    private $seniorityMap = [
        Vacancy::LEVEL_JUNIOR => 'A',
        Vacancy::LEVEL_MIDDLE => 'B',
        Vacancy::LEVEL_SENIOR => 'C',
        Vacancy::LEVEL_TECH_MANAGEMENT => 'D'
    ];

    /** @var ObjectHydrator */
    private $hydrator;

    public function __construct()
    {
        $this->hydrator = new ObjectHydrator();
    }

    /**
     * @param Vacancy[] $data
     * @param array     $orderBy
     *
     * @return Vacancy[]
     * @throws \Exception
     */
    public function orderWithSeniority(array $data, array $orderBy): array
    {
        $arrayData = [];
        foreach ($data as $item) {
            $arrayData[] = $this->hydrator->extract($item);
        }

        $mappedData = $this->mapSeniority($arrayData);

        $orderedMappedData = ArrayUtils::sortBy($mappedData, $orderBy);

        $remappedData = $this->remapSeniority($orderedMappedData);

        $entities = [];

        foreach ($remappedData as $item) {
            $entities[] = $this->hydrator->hydrate($item, new Vacancy());
        }

        return $entities;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function mapSeniority(array $data): array
    {
        $mapped = [];
        foreach ($data as $item) {
            if (isset($item[self::SENIORITY_LEVEL_KEY])) {
                $item[self::SENIORITY_LEVEL_KEY] = $this->seniorityMap[$item[self::SENIORITY_LEVEL_KEY]] .
                    '_' . $item[self::SENIORITY_LEVEL_KEY];
            }

            $mapped[] = $item;
        }

        return $mapped;
    }

    /**
     * @param array $mappedData
     *
     * @return array
     */
    private function remapSeniority(array $mappedData): array
    {
        $remapped = [];
        $prefixList = array_values($this->seniorityMap);
        $prefixListRegexp = implode('|', $prefixList);
        $regExp = "/($prefixListRegexp)_/";

        foreach ($mappedData as $item) {
            $item[self::SENIORITY_LEVEL_KEY] = preg_replace($regExp, '', $item[self::SENIORITY_LEVEL_KEY]);
            $remapped[] = $item;
        }

        return $remapped;
    }
}