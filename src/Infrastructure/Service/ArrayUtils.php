<?php

namespace App\Infrastructure\Service;

class ArrayUtils
{
    /**
     * @param array $data
     * @param array $params
     *
     * @return array
     */
    public static function sortBy(array $data, array $params): array
    {
        $key = $params[0];
        $odr = array_column($data, $key);

        array_multisort($odr, SORT_ASC, $data);

        return $data;
    }
}