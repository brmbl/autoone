<?php

namespace App\Infrastructure\Service;

use App\Domain\Entity\Vacancy;

class VacancyMatchingCalculator
{
    /** @var array */
    private $matchingTable = [];

    /**
     * @param Vacancy[] $vacancies
     * @param string    $skills
     *
     * @return Vacancy
     */
    public function calculateBySkills(array $vacancies, string $skills): Vacancy
    {
        $skillSet = explode(',', $skills);
        $requestSkillCount = count($skillSet);
        for ($i = 0; $i < $requestSkillCount; $i++) {
            $skillSet[$i] = trim($skillSet[$i], ', ');
        }

        foreach ($vacancies as $vacancy) {
            $vacancySkill = $vacancy->getRequiredSkills();
            $vacancySkillCount = count($vacancySkill);
            for ($i = 0; $i < $vacancySkillCount; $i++) {
                $vacancySkill[$i] = trim($vacancySkill[$i], ', ');
            }
            $intersect = array_intersect($skillSet, $vacancySkill);
            $intersectCount = count($intersect);

            $byRequest = round($intersectCount * 100 / $requestSkillCount);
            $byVacancy = round(($intersectCount * 100) / $vacancySkillCount);
            $marker = round(($byRequest + $byVacancy) / 2);
            $match = [
                'marker' => $marker,
                'salary' => $vacancy->getSalary(),
                'vacancy' => $vacancy
            ];
            $this->matchingTable[] = $match;
        }

        return $this->findBestOption();
    }

    /**
     * @return Vacancy
     */
    private function findBestOption(): Vacancy
    {
        $criteria1 = array_column($this->matchingTable, 'marker');
        $criteria2 = array_column($this->matchingTable, 'salary');

        array_multisort($criteria1, SORT_DESC, $criteria2, SORT_DESC, $this->matchingTable);
        return $this->matchingTable[0]['vacancy'];
    }

}