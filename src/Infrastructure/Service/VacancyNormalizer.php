<?php

namespace App\Infrastructure\Service;

use Symfony\Component\HttpFoundation\Request;

class VacancyNormalizer
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function normalizeRequest(Request $request): array
    {
        $criteria = [];
        $orderBy = [];
        if ($request->query->has('country')) {
            $criteria['Country'] = strtoupper($request->query->get('country'));
        }
        if ($request->query->has('city')) {
            $criteria['City'] = ucfirst($request->query->get('city'));
        }
        if ($request->query->has('orderBy')) {
            $orderBy[] = $request->query->get('orderBy') === 'seniority'
                ? VacancyFilter::SENIORITY_LEVEL_FIELD : 'Salary';
        }

        return [$criteria, $orderBy];
    }
}