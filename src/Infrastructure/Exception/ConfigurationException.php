<?php

namespace App\Infrastructure\Exception;

class ConfigurationException extends \Exception
{
    /**
     * @param array $params
     */
    public function __construct(array $params)
    {
        $params_list = implode($params);
        $message = "Next configuration params is missing: $params_list";
        parent::__construct($message);
    }
}