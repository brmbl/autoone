<?php

namespace App\Infrastructure\DBAL\FileManagement;


interface FileManagerInterface
{
    public function readBy(array $criteria, array $orderBy = []);

    public function write();

    public function delete();
}