<?php

namespace App\Infrastructure\DBAL\FileManagement;


use App\Infrastructure\DBAL\Configuration\CsvConfiguration;
use App\Infrastructure\Service\ArrayUtils;

class CsvManager implements FileManagerInterface
{
    /** @var string */
    private $filePath;

    /**
     * @param CsvConfiguration $configuration
     */
    public function __construct(CsvConfiguration $configuration)
    {
        $this->filePath = $configuration->getFilePath();
    }

    /**
     * @param array $criteria
     * @param array $orderBy
     *
     * @return array
     * @throws \Exception
     */
    public function readBy(array $criteria, array $orderBy = []): array
    {
        $arrayData = [];
        $fileHeaders = [];

        if (($handle = fopen($this->filePath, "r")) !== false) {
            while (($data = fgetcsv($handle, 1000)) !== false) {
                if (empty($fileHeaders)) {
                    $fileHeaders = $data;
                    continue;
                }
                $row = array_combine($fileHeaders, $data);
                if (!$row) {
                    throw new \Exception("Source file contains bad formatted data");
                }
                $intersect = array_intersect_assoc($row, $criteria);
                if (count($intersect) != count($criteria)) {
                    continue;
                }
                $arrayData[] = $row;
            }
            fclose($handle);

            if ($orderBy) {
                return ArrayUtils::sortBy($arrayData, $orderBy);
            }

            return $arrayData;
        }

        throw new \Exception("Could not open source file");
    }

    public function write()
    {
        // TODO: Implement write() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }
}