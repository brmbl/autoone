<?php

namespace App\Infrastructure\DBAL\Configuration;

use App\Infrastructure\Exception\ConfigurationException;

class DBALConfiguration implements DBALConfigurationInterface
{

    /** @var array */
    private $params;

    /**
     * @param array $params
     *
     * @throws ConfigurationException
     */
    public function __construct(array $params)
    {
        if (!isset($params['driver'])) {
            throw new ConfigurationException(['driver']);
        }

        $this->params = $params;
    }

    /**
     * @return string
     */
    public function getDriver(): string
    {
        return $this->params['driver'];
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

}