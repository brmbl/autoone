<?php

namespace App\Infrastructure\DBAL\Configuration;


use App\Infrastructure\Exception\ConfigurationException;

class CsvConfiguration
{
    /** @var string */
    private $filePath;

    public function __construct(array $configuration)
    {
        if (!isset($configuration['file_path'])){
            throw new ConfigurationException(['file_path']);
        }

        $this->filePath = $configuration['file_path'];
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }
}