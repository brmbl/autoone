<?php

namespace App\Infrastructure\DBAL\Configuration;


interface DBALConfigurationInterface
{
    public function getDriver(): string;

    public function getParams(): array;
}