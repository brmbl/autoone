<?php

namespace App\Infrastructure\ORM\Persister;

use App\Domain\Entity\Vacancy;
use App\Infrastructure\DBAL\FileManagement\CsvManager;
use App\Infrastructure\ORM\EntityManager;

class CsvPersister implements Persistable
{
    /** @var CsvManager */
    private $fileManager;

    /** @var EntityManager */
    private $em;

    /**
     * @param EntityManager $em
     * @param CsvManager    $fileManager
     */
    public function __construct(EntityManager $em, CsvManager $fileManager)
    {
        $this->em = $em;
        $this->fileManager = $fileManager;
    }

    /**
     * @param int $id
     *
     * @return Vacancy|null
     * @throws \Exception
     */
    public function loadById($id)
    {
        return $this->loadOne(['ID' => $id]);
    }

    /**
     * @param array $criteria
     *
     * @return Vacancy|null
     * @throws \Exception
     */
    public function loadOne(array $criteria)
    {
        $data = $this->loadBy($criteria);

        return isset($data[0]) ? $data[0] : null;
    }

    /**
     * @param array $criteria
     * @param array $orderBy
     *
     * @return array
     * @throws \Exception
     */
    public function loadBy(array $criteria, array $orderBy = []): array
    {
        $result = $this->fileManager->readBy($criteria, $orderBy);

        return $this->em->getHydrator()->hydrateAll($result, $this->em->getEntity());
    }

    /**
     * @throws \Exception
     */
    public function loadAll()
    {
        $result = $this->fileManager->readBy([], []);

        return $this->em->getHydrator()->hydrateAll($result, $this->em->getEntity());
    }
}