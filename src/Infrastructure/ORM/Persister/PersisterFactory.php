<?php

namespace App\Infrastructure\ORM\Persister;

use App\Infrastructure\DBAL\FileManagement\CsvManager;
use App\Infrastructure\DBAL\Configuration\CsvConfiguration;
use App\Infrastructure\ORM\EntityManager;

class PersisterFactory
{
    /**
     * @param EntityManager $em
     *
     * @return Persistable
     * @throws \Exception
     */
    public static function createPersister(EntityManager $em): Persistable
    {
        switch ($em->getConfiguration()->getDriver()) {
            case 'csv':
                $csvConfiguration = new CsvConfiguration($em->getConfiguration()->getParams());
                $fileManager = new CsvManager($csvConfiguration);
                return new CsvPersister($em, $fileManager);
                break;
            default:
                throw new \Exception('Undefined DBAL driver' . $em->getConfiguration()->getDriver());
        }
    }
}