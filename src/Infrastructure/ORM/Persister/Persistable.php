<?php

namespace App\Infrastructure\ORM\Persister;


interface Persistable
{
    public function loadById($id);

    public function loadOne(array $criteria);

    public function loadBy(array $criteria, array $orderBy = []): array ;

    public function loadAll();
}