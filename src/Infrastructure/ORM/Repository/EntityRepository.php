<?php

namespace App\Infrastructure\ORM\Repository;

use App\Domain\RepositoryInterface;
use App\Infrastructure\ORM\EntityManager;

abstract class EntityRepository implements RepositoryInterface
{
    /** @var string */
    protected $entityName;

    /** @var EntityManager */
    protected $em;

    /**
     * @param EntityManager $em
     * @param string        $entityName
     */
    public function __construct(EntityManager $em, $entityName)
    {
        $this->em = $em;
        $this->entityName = $entityName;
        $this->em->setEntityName($this->entityName);
    }
}