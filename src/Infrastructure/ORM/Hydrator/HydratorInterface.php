<?php

namespace App\Infrastructure\ORM\Hydrator;


interface HydratorInterface extends ExtractionInterface, HydrationInterface
{

}