<?php

namespace App\Infrastructure\ORM;

use App\Infrastructure\DBAL\Configuration\DBALConfigurationInterface;
use App\Infrastructure\ORM\Hydrator\ObjectHydrator;
use App\Infrastructure\ORM\Persister\Persistable;
use App\Infrastructure\ORM\Persister\PersisterFactory;

class EntityManager
{
    private $configuration;

    private $persister;

    private $entityName;

    /**
     * @param DBALConfigurationInterface $configuration
     * @throws \Exception
     */
    public function __construct(DBALConfigurationInterface $configuration)
    {
        $this->configuration = $configuration;
        $this->persister = PersisterFactory::createPersister($this);
    }

    /**
     * @param string $entityName
     */
    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;
    }

    public function getEntityName(): string
    {
        return $this->entityName;
    }

    public function getEntity()
    {
        return new $this->entityName;
    }

    public function getPersister(): Persistable
    {
        return $this->persister;
    }

    public function getHydrator()
    {
        return new ObjectHydrator();
    }

    /**
     * @return DBALConfigurationInterface
     */
    public function getConfiguration(): DBALConfigurationInterface
    {
        return $this->configuration;
    }
}