<?php

namespace App\Domain;


interface RepositoryInterface
{
    public function findAll(): array;

    public function findBy(array $criteria);

    public function findById(int $id);
}