<?php

namespace App\Domain\Entity;


class Vacancy implements \JsonSerializable
{
    const LEVEL_JUNIOR = 'Junior';

    const LEVEL_MIDDLE = 'Middle';

    const LEVEL_SENIOR = 'Senior';

    const LEVEL_TECH_MANAGEMENT = 'Tech management';

    private $id;

    private $jobTitle;

    private $seniorityLevel;

    private $country;

    private $city;

    private $salary;

    private $currency;

    private $requiredSkills;

    private $companySize;

    private $companyDomain;

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setID($id): Vacancy
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $jobTitle
     *
     * @return Vacancy
     */
    public function setJobTitle($jobTitle): Vacancy
    {
        $this->jobTitle = $jobTitle;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * @param string $seniorityLevel
     *
     * @return Vacancy
     */
    public function setSeniorityLevel($seniorityLevel): Vacancy
    {
        $this->seniorityLevel = $seniorityLevel;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeniorityLevel()
    {
        return $this->seniorityLevel;
    }

    /**
     * @param string $country
     *
     * @return Vacancy
     */
    public function setCountry($country): Vacancy
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $city
     *
     * @return Vacancy
     */
    public function setCity($city): Vacancy
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $salary
     *
     * @return Vacancy
     */
    public function setSalary($salary): Vacancy
    {
        $this->salary = $salary;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @param string $currency
     *
     * @return Vacancy
     */
    public function setCurrency($currency): Vacancy
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param array|string $requiredSkills
     *
     * @return Vacancy
     */
    public function setRequiredSkills($requiredSkills): Vacancy
    {
        if (is_array($requiredSkills)) {
            $this->requiredSkills = implode(',', $requiredSkills);
        } else {
            $this->requiredSkills = $requiredSkills;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getRequiredSkills()
    {
        return explode(',', $this->requiredSkills);
    }

    /**
     * @param string $companySize
     *
     * @return Vacancy
     */
    public function setCompanySize($companySize): Vacancy
    {
        $this->companySize = $companySize;
        return $this;
    }

    /**
     * @return string
     */
    public function getCompanySize()
    {
        return $this->companySize;
    }

    /**
     * @param string $companyDomain
     *
     * @return Vacancy
     */
    public function setCompanyDomain($companyDomain): Vacancy
    {
        $this->companyDomain = $companyDomain;
        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyDomain()
    {
        return $this->companyDomain;
    }

    public function jsonSerialize()
    {
        return [
            'ID' => $this->id,
            'Job title' => $this->jobTitle,
            'Seniority level' => $this->seniorityLevel,
            'Country' => $this->country,
            'City' => $this->city,
            'Salary' => $this->salary,
            'Currency' => $this->currency,
            'Required skills' => $this->requiredSkills,
            'Company size' => $this->companySize,
            'Company domain' => $this->companyDomain,
        ];
    }
}