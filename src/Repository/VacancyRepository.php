<?php

namespace App\Repository;

use App\Domain\Entity\Vacancy;
use App\Infrastructure\ORM\EntityManager;
use App\Infrastructure\ORM\Repository\EntityRepository;
use App\Infrastructure\Service\VacancyFilter;

class VacancyRepository extends EntityRepository// TODO: Implement loadAll() method.
{
    public function __construct(EntityManager $em, string $entityName)
    {
        parent::__construct($em, $entityName);
    }

    /**
     * @param int $id
     *
     * @return Vacancy|null
     */
    public function findById(int $id)
    {
        return $this->em->getPersister()->loadById($id);
    }

    /**
     * @param array $criteria
     * @param array $orderBy
     *
     * @return Vacancy[]
     * @throws \Exception
     */
    public function findBy(array $criteria, array $orderBy = []): array
    {
        $result = $this->em->getPersister()->loadBy($criteria, $orderBy);

        if (isset($orderBy[VacancyFilter::SENIORITY_LEVEL_FIELD])) {
            $filter = new VacancyFilter();
            $filter->orderWithSeniority($result, $orderBy = []);
        }

        return $result;
    }

    public function findAll(): array
    {
        return $this->em->getPersister()->loadAll();
    }
}