<?php

namespace App\Controller;


use App\Exception\ResourceNotFoundException;
use App\Infrastructure\Service\VacancyMatchingCalculator;
use App\Infrastructure\Service\VacancyNormalizer;
use App\Repository\VacancyRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class VacancyController extends FOSRestController
{
    /**
     * @Rest\Get("/vacancies/{id}", requirements={"id" = "\d+"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns the certain vacancy by ID",
     * )
     * @SWG\Parameter(
     *     name="id",
     *     required=true,
     *     pattern="\d+",
     *     in="path",
     *     type="integer",
     *     description="The unique identifier of vacancy"
     * )
     * @param int               $id
     *
     * @param VacancyRepository $repository
     *
     * @return JsonResponse
     * @throws ResourceNotFoundException
     */
    public function getOneVacancyAction(int $id, VacancyRepository $repository): JsonResponse
    {
        $vacancy = $repository->findById($id);

        if (empty($vacancy)) {
            throw new ResourceNotFoundException("Vacancy with ID $id not found");
        }

        return new JsonResponse($vacancy);
    }

    /**
     * @Rest\Get("/vacancies")
     * @Rest\QueryParam(name="country", nullable=true, strict=true, requirements="[A-z]{2}", description="To filter by country")
     * @Rest\QueryParam(name="city", nullable=true, strict=true, requirements="[A-z]+", description="To filter by city")
     * @Rest\QueryParam(name="orderBy", nullable=true, strict=true, requirements="(seniority|salary)")
     * @SWG\Response(
     *     response=200,
     *     response=400,
     *     description="Returns the list of vacancies",
     * )
     * @param Request           $request
     *
     * @param VacancyRepository $repository
     * @param VacancyNormalizer $normalizer
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function getVacanciesAction
    (
        Request $request,
        VacancyRepository $repository,
        VacancyNormalizer $normalizer
    ): JsonResponse {
        list($criteria, $orderBy) = $normalizer->normalizeRequest($request);

        $vacancies = $repository->findBy($criteria, $orderBy);

        return new JsonResponse($vacancies);
    }

    /**
     * @Rest\Get("/vacancies/match")
     * @Rest\QueryParam(name="skills", nullable=false, strict=true, requirements="[a-zA-Z-_\/]+(,[a-zA-Z-_\/]+)*")
     * @SWG\Response(
     *     response=200,
     *     description="Returns the most interesting position for a candidate with a certain set of skills",
     * )
     * @SWG\Parameter(
     *     name="skills",
     *     in="query",
     *     type="string",
     *     description="The set of skills, separated by comma"
     * )
     * @param Request                   $request
     * @param VacancyRepository         $repository
     * @param VacancyMatchingCalculator $calculator
     *
     * @return JsonResponse
     */
    public function matchVacanciesAction(
        Request $request,
        VacancyRepository $repository,
        VacancyMatchingCalculator $calculator
    ) {
        $vacancies = $repository->findAll();

        if (empty($vacancies)) {
            return new JsonResponse([]);
        }

        $bestVacancy = $calculator->calculateBySkills($vacancies, $request->query->get('skills'));

        return new JsonResponse($bestVacancy);
    }
}